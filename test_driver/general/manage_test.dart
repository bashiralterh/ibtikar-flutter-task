import 'dart:developer';

import 'package:flutter_driver/flutter_driver.dart';

import 'widgets_items.dart';

class ManageTest {

  openImageAndDownloadIt(FlutterDriver? driver) async {
    log('Test: Waiting for itemCard shown');

    await Future.delayed(const Duration(seconds: 4));

    await driver?.waitFor(widgetsItems.itemCard);

    await Future.delayed(const Duration(seconds: 4));

    await driver?.tap(widgetsItems.itemCard);

    await Future.delayed(const Duration(seconds: 4));

    log('Test: Waiting for itemImagesGrid shown');
    await driver?.waitFor(widgetsItems.itemImagesGrid,timeout: const Duration(seconds: 10));

    log('Test: Waiting for itemCard shown');
    await driver?.waitFor(widgetsItems.itemImage,timeout: const Duration(seconds: 10));

    await Future.delayed(const Duration(seconds: 2));

    await driver?.tap(widgetsItems.itemImage);

    log('Test: Waiting for btnDownloadImage shown');
    await driver?.waitFor(widgetsItems.btnDownloadImage);

    await Future.delayed(const Duration(seconds: 2));

    await driver?.tap(widgetsItems.btnDownloadImage);

    await Future.delayed(const Duration(seconds: 10));

  }


}

final manageTest = ManageTest();
