import 'package:flutter_driver/flutter_driver.dart';

class WidgetsItems{
  final itemCard = find.byValueKey('itemCardKey');
  final itemImage = find.byValueKey('itemImageKey');
  final itemImagesGrid = find.byValueKey('itemImagesGridKey');
  final btnDownloadImage = find.byValueKey('btnDownloadImageKey');

}

final widgetsItems = WidgetsItems();