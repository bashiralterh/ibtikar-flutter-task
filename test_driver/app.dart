import 'dart:developer';

import 'package:flutter_driver/driver_extension.dart';
import 'package:ibtikar_flutter_task/main.dart' as app;

void main() {
  // This line enables the extension.
  log('Integration test starting...');
  enableFlutterDriverExtension();
  // Call the `main()` function of the app, or call `runApp` with
  // any widget you are interested in testing.
  app.main();
}