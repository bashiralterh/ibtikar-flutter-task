import 'dart:developer';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

import 'general/manage_test.dart';

///Execute test -> put it in terminal  "flutter drive --target=test_driver/app.dart"


///View list --> Click on first item --> Open image --> download image


void main() {
  log('app_test starting...');

  group('Test Ibtikar task', () {

    FlutterDriver? driver;

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
      await driver?.waitUntilFirstFrameRasterized();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        driver?.close();
      }
    });

    ///Test-----
    test('Test Ibtikar', () async {
      await driver?.runUnsynchronized(() async {
        await manageTest.openImageAndDownloadIt(driver);
        await Future.delayed(const Duration(seconds: 10));
      });
    });

  }, timeout: Timeout.none);
}
