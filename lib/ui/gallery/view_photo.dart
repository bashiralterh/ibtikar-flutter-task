import 'dart:io';

import 'package:flutter/material.dart';
import 'package:ibtikar_flutter_task/application/app_constant.dart';
import 'package:ibtikar_flutter_task/model/person_images_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

import '../../BLoC/general_BLoC.dart';
import '../../application/widget_keys.dart';

class ViewPhotos extends StatefulWidget {
  const ViewPhotos({Key? key, required this.imageIndex, required this.imageList, this.heroTitle = "img"})
      : super(key: key);

  final String heroTitle;
  final int imageIndex;
  final List<ImageItem> imageList;

  @override
  _ViewPhotosState createState() => _ViewPhotosState();
}

class _ViewPhotosState extends State<ViewPhotos> {
  late PageController pageController;
  late int currentIndex;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    currentIndex = widget.imageIndex;
    pageController = PageController(initialPage: widget.imageIndex);
  }

  void onPageChanged(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text(
          "${currentIndex + 1} out of ${widget.imageList.length}",
          style: const TextStyle(color: Colors.white),
        ),
        actions: [
          _downloadWidget(),
          IconButton(
            icon: const Icon(
              Icons.clear,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          )
        ],
        centerTitle: true,
        leading: Container(),
        backgroundColor: Colors.black,
      ),
      body: Stack(
        children: [
          PhotoViewGallery.builder(
            scrollPhysics: const BouncingScrollPhysics(),
            pageController: pageController,
            builder: (BuildContext context, int index) {
              return PhotoViewGalleryPageOptions(
                imageProvider: NetworkImage('${AppConstant.IMAGE_URL}${widget.imageList[index].filePath}'),
                heroAttributes: PhotoViewHeroAttributes(tag: "photo${widget.imageIndex}"),
              );
            },
            onPageChanged: onPageChanged,
            itemCount: widget.imageList.length,
            loadingBuilder: (context, progress) => Center(
              child: SizedBox(
                width: 60.0,
                height: 60.0,
                child: CircularProgressIndicator(
                  value: progress == null ? null : _getProgressValue(progress),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  double _getProgressValue(ImageChunkEvent progress) {
    int? totalByte = progress.expectedTotalBytes ?? 0;

    return progress.cumulativeBytesLoaded / totalByte;
  }

  Widget _downloadWidget() {
    return IconButton(
      key: currentIndex == 0 ? WidgetKeys.btnDownloadImageKey : Key('btnDownloadImageKey$currentIndex'),
      icon: const Icon(
        Icons.download_sharp,
        color: Colors.white,
      ),
      onPressed: () async {
        String path = await _getPath();
        bloc.download('${AppConstant.IMAGE_URL}${widget.imageList[currentIndex].filePath}', path);
      },
    );
  }

  Future<String> _getPath() async {
    final _externalDirectory = await getExternalStorageDirectory();
    var dir = await Directory('${_externalDirectory?.path}/Download').create();

    var path = '${dir.path}/${widget.imageList[currentIndex].filePath.split('/').last}';
    return path;
  }
}
