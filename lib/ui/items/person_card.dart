// ignore_for_file: must_be_immutable

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:ibtikar_flutter_task/application/app_icons.dart';
import 'package:ibtikar_flutter_task/application/enums.dart';
import 'package:ibtikar_flutter_task/application/widget_keys.dart';
import 'package:ibtikar_flutter_task/localization/app_local.dart';
import 'package:ibtikar_flutter_task/managers/pages_routes.dart';
import 'package:ibtikar_flutter_task/model/people_model.dart';

import '../../application/app_colors.dart';
import '../../application/app_constant.dart';
import '../../application/app_text_style.dart';
import '../../widgets/custom_shimmer.dart';
import '../customs/person_details.dart';

class PersonCard extends StatefulWidget {
  PersonCard({
    Key? key,
    this.index = 0,
    required this.actorItem,
  }) : super(key: key);

  int? index;
  ActorItem? actorItem;

  @override
  _PersonCardState createState() => _PersonCardState();
}

class _PersonCardState extends State<PersonCard> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: Card(
        elevation: 5,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        child: InkWell(
            key: widget.index == 0 ? WidgetKeys.itemCardKey : Key('itemCardKey${widget.index}'),
            hoverColor: _getHoverColor(),
            focusColor: _getHoverColor(),
            onTap: () {
              ///Todo: inherited widget or injectable instead of constractor
              PagesRoutes.goToPage(
                  context,
                  PersonDetails(
                    index: widget.index!,
                    actorItem: widget.actorItem,
                  ));
            },
            borderRadius: const BorderRadius.all(
              Radius.circular(10),
            ),
            child: Stack(
              children: <Widget>[
                _effectWidget(),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    _profileImageWidget(),
                    const SizedBox(
                      width: 12,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        const SizedBox(
                          height: 12,
                        ),
                        _mainInfoWidget(),
                        const SizedBox(
                          height: 8,
                        ),
                        Container(
                            width: MediaQuery.of(context).size.width * 0.6,
                            height: 1,
                            color: AppColors.black.withOpacity(0.7)),
                        const SizedBox(
                          height: 8,
                        ),
                        _knownForMoviesWidget(),
                        const SizedBox(
                          height: 12,
                        ),
                      ],
                    )
                  ],
                ),
              ],
            )),
      ),
    );
  }

  Color _getHoverColor() {
    return widget.actorItem?.gender == Gender.female.value ? AppColors.pink : AppColors.customGreen;
  }

  Widget _mainInfoWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        AutoSizeText(
          widget.actorItem?.name ?? '--',
          style: AppTextStyle.mediumBlackBold,
          overflow: TextOverflow.ellipsis,
        ),
        Row(
          children: [
            RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                    text: '${AppLocalizations.of(context).trans('known_for')}:',
                    style: AppTextStyle.smallGrayBold,
                  ),
                  TextSpan(
                    text: widget.actorItem?.knownForDepartment ?? '--',
                    style: AppTextStyle.smallGray,
                  )
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _profileImageWidget() {
    return SizedBox(
      height: 115,
      width: 90,
      child: Hero(
        tag: 'personTag${widget.index}',
        child: ClipRRect(
          borderRadius: const BorderRadius.only(topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
          child: CachedNetworkImage(
            width: 75,
            height: 75,
            imageUrl: '${AppConstant.IMAGE_URL}/${widget.actorItem?.profilePath ?? ''}',
            progressIndicatorBuilder: (context, url, downloadProgress) => CustomShimmer.squarer(),
            fit: BoxFit.cover,
            errorWidget: (context, url, error) => Image.asset(
              AppIcons.imagePlaceHolder.assetName,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }

  Widget _knownForMoviesWidget() {
    return SizedBox(
        width: MediaQuery.of(context).size.width * 0.57,
        child: Text(_getKnownForMovies(), overflow: TextOverflow.ellipsis, style: AppTextStyle.smallGray, maxLines: 2));
  }

  String _getKnownForMovies() {
    String items = '';
    for (KnownFor element in widget.actorItem?.knownFor ?? []) {
      items += element.title;
    }
    return items;
  }

  Widget _effectWidget() {
    return Align(
      alignment: Alignment.topRight,
      child: ClipRRect(
          borderRadius: const BorderRadius.only(topRight: Radius.circular(10),),
          child: Image.asset(
        widget.actorItem?.gender == Gender.female.value ? AppIcons.pinkEffect.assetName : AppIcons.blueEffect.assetName,
        fit: BoxFit.fill,
        width: 85.17,
        height: 70.5,
      )),
    );
  }
}
