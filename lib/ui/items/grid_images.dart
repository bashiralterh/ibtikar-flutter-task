// ignore_for_file: must_be_immutable

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:ibtikar_flutter_task/application/app_constant.dart';
import 'package:ibtikar_flutter_task/managers/pages_routes.dart';

import '../../application/widget_keys.dart';
import '../../model/person_images_model.dart';
import '../../widgets/custom_shimmer.dart';
import '../gallery/view_photo.dart';

class GridImages extends StatelessWidget {
  GridImages({required this.images, Key? key}) : super(key: key);

  List<ImageItem> images;

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      key: WidgetKeys.itemImagesGridKey,
      shrinkWrap: true,
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3, childAspectRatio: 1.2, crossAxisSpacing: 2.2, mainAxisSpacing: 15),
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 4,
        ),
        child: _image(context, images[index], index: index, onTap: () {
          PagesRoutes.goToPage(
              context,
              ViewPhotos(
                imageList: images,
                imageIndex: index,
              ));
        }),
      ),
      itemCount: images.length,
    );
  }

  Widget _image(BuildContext context, ImageItem image, {required Function onTap, required int index}) {
    return GestureDetector(
        key: index == 0 ? WidgetKeys.itemImageKey : Key('itemImageKey$index'),
        onTap: () {
          onTap();
        },
        child: CachedNetworkImage(
          width: 75,
          height: 75,
          imageUrl: '${AppConstant.IMAGE_URL}/${image.filePath}',
          progressIndicatorBuilder: (context, url, downloadProgress) => CustomShimmer.squarer(),
          fit: BoxFit.cover,
          errorWidget: (context, url, error) => const Icon(Icons.error_sharp),
        ));
  }
}
