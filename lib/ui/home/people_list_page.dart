import 'package:flutter/material.dart';
import 'package:ibtikar_flutter_task/BLoC/popular_list_BLoC.dart';
import 'package:ibtikar_flutter_task/localization/app_local.dart';
import 'package:ibtikar_flutter_task/widgets/helper_widgets.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../model/people_model.dart';
import '../items/person_card.dart';

class PeopleListPage extends StatefulWidget {
  const PeopleListPage({Key? key}) : super(key: key);

  @override
  _PeopleListPageState createState() => _PeopleListPageState();
}

class _PeopleListPageState extends State<PeopleListPage> with TickerProviderStateMixin {
  int _currentPageNumber = 1;

  final PagingController<int, ActorItem> _pagingController = PagingController(firstPageKey: 0);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage();
    });
  }

  Future<void> _fetchPage() async {
    try {
      popularListBloc.getPopularPeople(context, _currentPageNumber++, onGetResult: ({value, error}) {
        final isLastPage = _currentPageNumber >= value!.totalPages;
        if (isLastPage) {
          _pagingController.appendLastPage(value.results);
        } else {
          _pagingController.appendPage(value.results, _currentPageNumber);
        }
      });
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: helperWidgets.appBar(context, AppLocalizations.of(context).trans('popular_people')),
      body: StreamBuilder<PeopleModel>(
          stream: popularListBloc.getPopularPersonStream,
          builder: (context, snapshot) {
            return _listWidget();
          }),
    );
  }

  Widget _listWidget() {
    return PagedListView<int, ActorItem>(
      physics: const BouncingScrollPhysics(),
      pagingController: _pagingController,
      builderDelegate: PagedChildBuilderDelegate<ActorItem>(
        itemBuilder: (context, item, index) => PersonCard(index: index, actorItem: item),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _pagingController.dispose();
  }
}
