// ignore_for_file: must_be_immutable

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:ibtikar_flutter_task/BLoC/person_details_BLoC.dart';
import 'package:ibtikar_flutter_task/application/app_icons.dart';
import 'package:ibtikar_flutter_task/model/person_details_model.dart';
import 'package:ibtikar_flutter_task/model/person_images_model.dart';
import 'package:ibtikar_flutter_task/widgets/helper_widgets.dart';

import '../../application/app_colors.dart';
import '../../application/app_constant.dart';
import '../../application/app_text_style.dart';
import '../../application/enums.dart';
import '../../localization/app_local.dart';
import '../../model/people_model.dart';
import '../../widgets/animation_scale_widget.dart';
import '../../widgets/custom_shimmer.dart';
import '../items/grid_images.dart';

class PersonDetails extends StatefulWidget {
  PersonDetails({Key? key, required this.index, required this.actorItem}) : super(key: key);

  int index;
  ActorItem? actorItem;

  @override
  _PersonDetailsState createState() => _PersonDetailsState();
}

class _PersonDetailsState extends State<PersonDetails> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _fetchData();
  }

  void _fetchData() {
    personDetailsBloc.getPersonDetails(
      context,
      widget.actorItem?.id,
    );

    personDetailsBloc.getPersonImages(context, widget.actorItem?.id);
  }

  @override
  Widget build(BuildContext context) {
    return Container(color: AppColors.white,child:Stack(
      children: [
        _effectWidget(),
        Scaffold(
          backgroundColor: Colors.transparent,
            appBar: helperWidgets.appBar(context,'',withBack: true,onBackTap: (){
              Navigator.pop(context);
            }),
            body: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 8,
                    ),
                    _mainInfoWidget(),
                    _biographyWidget(),
                    _imagesWidget(),
                    const SizedBox(
                      height: 32,
                    ),
                  ],
                ))),
      ],
    ));
  }


  Widget _mainInfoWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(
          width: 12,
        ),
        _photoWidget(),
        const SizedBox(
          width: 12,
        ),
        Expanded(
          flex: 3,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              const SizedBox(
                height: 8,
              ),
              AnimationScaleWidget(
                position: 0,
                horizontalOffset: 10,
                verticalOffset: 0,
                child: AutoSizeText(
                  widget.actorItem?.name ?? '-',
                  style: AppTextStyle.largeBlackBold,
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              _otherInfo(),
            ],
          ),
        ),
      ],
    );
  }

  Widget _biographyWidget() {
    return StreamBuilder<PersonDetailsModel>(
        stream: personDetailsBloc.getPersonDetailsStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data?.biography.isEmpty ?? true) {
              return const SizedBox.shrink();
            }
            return Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      AppLocalizations.of(context).trans('biography'),
                      textAlign: TextAlign.start,
                      style: AppTextStyle.largeGrayBold,
                    ),
                    helperWidgets.textReadMore(
                        text: snapshot.data?.biography ?? '--',
                        collapsedText: AppLocalizations.of(context).trans('see_more'),
                        expandedText: AppLocalizations.of(context).trans('see_less'))
                  ],
                ));
          } else {
            return const DetailsLoading();
          }
        });
  }

  Widget _imagesWidget() {
    return StreamBuilder<PersonImagesModel>(
        stream: personDetailsBloc.getPersonImagesStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(padding: EdgeInsets.symmetric(horizontal: 16.0), child: Divider()),
                _photosTitleWidget(snapshot.data?.images.length??0),
                GridImages(
                  images: snapshot.data?.images ?? [],
                )
              ],
            );
          } else {
            return const GridLoading();
          }
        });
  }

  Widget _photosTitleWidget(int imageCount) {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        child: Text(
          AppLocalizations.of(context)
              .trans('${widget.actorItem?.name}\'s ${AppLocalizations.of(context).trans('photos')} ($imageCount)'),
          textAlign: TextAlign.start,
          style: AppTextStyle.mediumBlack,
        ));
  }

  Widget _otherInfo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _titleAndTextWidget(
            title: '${AppLocalizations.of(context).trans('known_for')}: ',
            text: widget.actorItem?.knownForDepartment ?? '--',
            position: 1),
        _titleAndTextWidget(
            title: '${AppLocalizations.of(context).trans('gender')}: ', text: _getGender(), position: 2),
        _titleAndTextWidget(
            title: '${AppLocalizations.of(context).trans('popularity')}: ',
            text: '${widget.actorItem?.popularity.toStringAsFixed(2)}',
            position: 2),
        StreamBuilder<PersonDetailsModel>(
            stream: personDetailsBloc.getPersonDetailsStream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return _titleAndTextWidget(
                    title: '${AppLocalizations.of(context).trans('birthday')}: ',
                    text: snapshot.data?.birthday ?? '--',
                    position: 2);
              } else {
                return const TextLoading();
              }
            })
      ],
    );
  }

  Widget _effectWidget() {
    return Align(
      alignment: Alignment.topRight,
      child: ClipRRect(
          borderRadius: const BorderRadius.only(
            topRight: Radius.circular(10),
          ),
          child: Image.asset(
            widget.actorItem?.gender == Gender.female.value
                ? AppIcons.pinkEffect.assetName
                : AppIcons.blueEffect.assetName,
            fit: BoxFit.fill,
            width: 150.17 * 1.2,
            height: 130.5 * 1.2,
          )),
    );
  }

  Widget _titleAndTextWidget({required String title, required String text, int position = 0}) {
    return AnimationScaleWidget(
        horizontalOffset: 0,
        verticalOffset: 10,
        position: position,
        child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 4.0),
            child: RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                    text: title,
                    style: AppTextStyle.mediumGrayBold,
                  ),
                  TextSpan(
                    text: text,
                    style: AppTextStyle.mediumGray,
                  )
                ],
              ),
            )));
  }

  String _getGender() {
    return AppLocalizations.of(context).trans(widget.actorItem?.gender == 1 ? 'female' : 'male');
  }

  Widget _photoWidget() {
    return Expanded(
      flex: 2,
      child: Hero(
        tag: 'personTag${widget.index}',
        child: ClipRRect(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          child: SizedBox(
            height: 200,
            child: CachedNetworkImage(
              imageUrl: '${AppConstant.IMAGE_URL}${widget.actorItem?.profilePath ?? ''}',
              progressIndicatorBuilder: (context, url, downloadProgress) => CustomShimmer.squarer(),
              fit: BoxFit.cover,
              errorWidget: (context, url, error) => Image.asset(
                AppIcons.imagePlaceHolder.assetName,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
