// ignore_for_file: file_names

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:ibtikar_flutter_task/model/people_model.dart';
import 'package:ibtikar_flutter_task/services/api_provider.dart';
import 'package:rxdart/rxdart.dart';
import '../managers/data_store.dart';
import 'general_BLoC.dart';

class PopularListBLoC extends GeneralBLoC {
  PopularListBLoC.internal() : super.internal();

  final _getPopularPeopleController = PublishSubject<PeopleModel>();

  get getPopularPersonStream => _getPopularPeopleController.stream;


  getPopularPeople(BuildContext context, int pageNumber,
      {required Function({PeopleModel? value, dynamic error})? onGetResult}) {
    fetchData(apiProvider.getPopularPeople(context, pageNumber), onData: (value) {

      ///Save response in shared preferences
      dataStore.setPeople(value, pageNumber);

      _getPopularPeopleController.sink.add(value);
      if(onGetResult != null) {
        onGetResult(value: value);
      }
    }, onError: (error) {
      log('errrror:$error');
      ///Get response in shared preferences
      dataStore.getPeople(pageNumber).then((value) {
        log('.getPeople: ${value?.toJson()}');
        if(value != null) {
          onGetResult!(value: value);
          _getPopularPeopleController.sink.add(value);

        } else {
          onGetResult!(error: error);
          _getPopularPeopleController.sink.addError(error);

        }


      });
    });
  }


}

final popularListBloc = PopularListBLoC.internal();
