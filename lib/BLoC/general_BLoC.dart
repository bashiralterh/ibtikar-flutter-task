// ignore_for_file: file_names

import 'dart:developer';

import 'package:rxdart/rxdart.dart';

import '../services/api_provider.dart';

class GeneralBLoC {

  final _listenController = PublishSubject<bool>();

  Stream<bool> get listenStream => _listenController.stream;


  GeneralBLoC.internal();


  listen() {
    _listenController.sink.add(true);
  }

  fetchData(Future<dynamic> func,
      {required void Function(dynamic data) onData, required void Function(dynamic error) onError}) {
    func.then((value) {
      log('fetchData done: $value');
      if(value == null) {
        onError('Error');
      } else {
        onData(value);
      }
    }).onError((error, stackTrace) {
      onError(error);
    });
  }

  download(String url,String path){
    apiProvider.downloadFile(url, path);
  }

}

final bloc = GeneralBLoC.internal();
