// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:ibtikar_flutter_task/model/person_details_model.dart';
import 'package:ibtikar_flutter_task/services/api_provider.dart';
import 'package:rxdart/rxdart.dart';
import '../model/person_images_model.dart';
import 'general_BLoC.dart';

class PersonDetailsBLoC extends GeneralBLoC {
  PersonDetailsBLoC.internal() : super.internal();

  final _getPersonDetailsController = PublishSubject<PersonDetailsModel>();

  get getPersonDetailsStream => _getPersonDetailsController.stream;

  final _getPersonImagesController = PublishSubject<PersonImagesModel>();

  get getPersonImagesStream => _getPersonImagesController.stream;



  getPersonDetails(BuildContext context, int? id,
      {Function({PersonDetailsModel? value, dynamic error})? onGetResult}) {
    fetchData(apiProvider.getPersonDetails(context, id), onData: (value) {
      _getPersonDetailsController.sink.add(value);
      if(onGetResult != null) {
        onGetResult(value: value);
      }
    }, onError: (error) {
      _getPersonDetailsController.sink.addError(error);
      if(onGetResult != null) {
        onGetResult(error: error);
      }
    });
  }

  getPersonImages(BuildContext context, int? id,
      {Function({PersonImagesModel? value, dynamic error})? onGetResult}) {
    fetchData(apiProvider.getPersonImages(context, id), onData: (value) {
      _getPersonImagesController.sink.add(value);
      if(onGetResult != null) {
        onGetResult(value: value);
      }
    }, onError: (error) {
      _getPersonImagesController.sink.addError(error);
      if(onGetResult != null) {
        onGetResult(error: error);
      }
    });
  }


}

final personDetailsBloc = PersonDetailsBLoC.internal();
