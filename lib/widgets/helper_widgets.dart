// ignore_for_file: import_of_legacy_library_into_null_safe
import 'package:flutter/material.dart';
import 'package:ibtikar_flutter_task/application/app_colors.dart';
import 'package:ibtikar_flutter_task/application/app_text_style.dart';
import 'package:readmore/readmore.dart';

import '../application/app_icons.dart';
import '../localization/app_local.dart';

class HelperWidgets {


  Widget textReadMore({required String? text, required String collapsedText, required String expandedText}) {
    return ReadMoreText(
      text ?? '',
      trimLines: 3,
      colorClickableText: AppColors.customGreen,
      trimMode: TrimMode.Line,
      style: AppTextStyle.mediumGray,
      trimCollapsedText: collapsedText,
      trimExpandedText: expandedText,
      // moreStyle: const TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
    );
  }

  AppBar appBar(BuildContext context, String text, {bool withBack = false, Function? onBackTap}) {
    return AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Text(
          text,
          style: const TextStyle(color: AppColors.black),
        ),
        leadingWidth: 80,
        leading: (withBack) ? _backWidget(context, onBackTap: onBackTap) : null);
  }

  Widget _backWidget(BuildContext context, {Function? onBackTap}) {
    return TextButton(
      onPressed: () {
        if (onBackTap != null) {
          onBackTap();
        }
      },
      child: Row(
        children: <Widget>[
          const SizedBox(
            width: 8,
          ),
          Image.asset(
            AppIcons.back.assetName,
            height: 13.5,
            width: 18,
          ),
          const SizedBox(
            width: 8,
          ),
          Text(
            AppLocalizations.of(context).trans('Back'),
            style: const TextStyle(fontWeight: FontWeight.w300, fontSize: 12, color: AppColors.gray),
          )
        ],
      ),
    );
  }

  offlineWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          AppLocalizations.of(context).trans('offline_mode'),
        ),
        const SizedBox(width: 10),
        const Icon(Icons.signal_wifi_statusbar_connected_no_internet_4_sharp,color: AppColors.white),
      ],
    );
  }
}

final helperWidgets = HelperWidgets();
