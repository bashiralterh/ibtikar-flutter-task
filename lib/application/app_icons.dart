import 'package:flutter/material.dart';

class AppIcons{
  static Image getIcon(AssetImage icon,{double width=20,double height = 20,Color? color}){
    return Image(image: icon, height: height, width: width,color: color,);
  }

  static const AssetImage logo = AssetImage('assets/images/logo.png');
  static const AssetImage blueEffect = AssetImage('assets/images/blue_effect.png');
  static const AssetImage pinkEffect = AssetImage('assets/images/pink_effect.png');
  static const AssetImage imagePlaceHolder = AssetImage('assets/images/image_placeholder.png');
  static const AssetImage back = AssetImage('assets/images/ic_back.png');


}