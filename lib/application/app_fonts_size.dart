// ignore_for_file: constant_identifier_names

import 'package:flutter/material.dart';


abstract class AppFontsSize {
  static const double xx_small_font_size = 9.0;
  static const double x_small_font_size = 10.0;
  static const double halfx_small_font_size = 11.5;
  static const double small_font_size = 13.0;
  static const double semi_medium_font_size = 15.0;
  static const double medium_font_size = 18.0;
  static const double normal_font_size = 20.0;
  static const double large_font_size = 24.0;
  static const double x_large_font_size = 30.0;
  static const double xx_large_font_size = 50.0;
  static const double xxx_large_font_size = 65.0;

  static getScale(langCode, screenSize) {
    double scale = 1;

    if (screenSize != null) {
      if (screenSize.width <= 320) {
        scale = 1;
      } else if (screenSize.width > 320) {
        scale = 1;
      } else {
        scale = 1.0;
      }
    }

    if (langCode == "ar") {
      scale *= 1;
    } else {
      scale *= 1;
    }

    return scale;
  }

   static getScaledFontSize(double fontSize, {Size? screenSize}) =>
      fontSize * getScale('en', screenSize);
}
