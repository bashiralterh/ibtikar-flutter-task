enum DataStoreKeys { people, token, lang }

enum Gender { male, female }

extension GenderExtension on Gender {
  int get value {
    switch (this) {
      case Gender.male:
        return 2;
      case Gender.female:
        return 1;
    }
  }
}
