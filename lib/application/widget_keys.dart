import 'package:flutter/material.dart';

class WidgetKeys{

  static var itemCardKey = const Key('itemCardKey');
  static var itemImageKey = const Key('itemImageKey');
  static var itemImagesGridKey = const Key('itemImagesGridKey');
  static var btnDownloadImageKey = const Key('btnDownloadImageKey');

}