import 'package:flutter/material.dart';

import 'app_colors.dart';
import 'app_fonts_size.dart';

abstract class AppTextStyle {





//  ---------------------- black

  static final largeBlackBold = TextStyle(
      fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.large_font_size),
      color: AppColors.black,
      fontWeight: FontWeight.bold
  );

  static final normalBlackBold = TextStyle(
    color: AppColors.black,
    fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.normal_font_size),
    fontWeight: FontWeight.bold,
  );

  static final normalBlack = TextStyle(
    color: AppColors.black,
    fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.normal_font_size),
  );

  static final mediumBlack = TextStyle(
    color: AppColors.black,
    fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.medium_font_size),
  );

  static final mediumBlackBold = TextStyle(
    color: AppColors.black,
    fontWeight: FontWeight.bold,
    fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.medium_font_size),
  );

  static final mediumTextDarkBold = TextStyle(
    color: AppColors.textDark,
    fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.medium_font_size),
    fontWeight: FontWeight.bold,
  );

  static final semiMediumBlackBold = TextStyle(
    fontWeight: FontWeight.bold,
    color: AppColors.black,
    fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.semi_medium_font_size),
  );

  static final semiMediumBlack = TextStyle(
    color: AppColors.black,
    fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.semi_medium_font_size),
  );


  // gray
  static final mediumGray = TextStyle(
    color: AppColors.lightGray,
    fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.medium_font_size),
  );

  static final mediumGrayBold = TextStyle(
    color: AppColors.lightGray,
    fontWeight: FontWeight.bold,
    fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.medium_font_size),
  );


  static final largeGrayBold = TextStyle(
    color: AppColors.gray,
    fontWeight: FontWeight.bold,
    fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.large_font_size),
  );

  static final xLargeGray = TextStyle(
    color: AppColors.gray,
    fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.x_large_font_size),
  );

  static final xLargeGrayBold = TextStyle(
    color: AppColors.gray,
    fontWeight: FontWeight.bold,
    fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.x_large_font_size),
  );

  static final smallGray = TextStyle(
    color: AppColors.gray,
    fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.small_font_size),
  );

  static final smallGrayBold = TextStyle(
    color: AppColors.gray,
    fontWeight: FontWeight.bold,
    fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.small_font_size),
  );

  static final halfXSmallGray = TextStyle(
    color: AppColors.gray,
    fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.halfx_small_font_size),
  );

  static final xSmallGray = TextStyle(
    color: AppColors.gray,
    fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.x_small_font_size),
  );

  static final xxSmallGray = TextStyle(
    color: AppColors.gray,
    fontSize: AppFontsSize.getScaledFontSize(AppFontsSize.xx_small_font_size),
  );

}
