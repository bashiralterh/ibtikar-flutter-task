// ignore_for_file: use_full_hex_values_for_flutter_colors

import 'package:flutter/material.dart';

class AppColors {
  static const white = Color(0xFFffffff);
  static const whiteGray = Color(0xFFF7F7F7);
  static const black = Color(0xFF000000);
  static const blackThin = Color(0xFF4B4B4B);
  static const semiBlack = Color(0xFF6F6F6F);
  static const semiBlack2 = Color(0xFFF6F6F6);
  static const lightBlack = Color(0xFF707070);
  static const lightGray = Color(0xFF707070);
  static const darkGray = Color(0xFF4D4F5C);
  static const lightGray2 = Color(0xFFF1F1F1);
  static const blue = Color(0xFF004481);
  static const red = Colors.red;
  static const redB = Color(0xFFDB0000);
  static const grayCC = Color(0xFFC1C1C1);
  static const pink = Color(0xFF9F057D);
  static const golden = Color(0xFFF1B510);
  static const textDark = Color(0xFF6F6F6F);
  static const green = Color(0xFF2EC766);
  static const green2 = Color(0xFF25B0AC);
  static const darkGreen = Color(0xFF25B038);
  static const bgColor = Color(0xFFefefef);
  static const textLight = Color(0xFFc1c2c5);
  static const textLightExtra = Color(0xFFe7e7e7);
  static const yellow = Color(0xFFF1B510);
  static const textBlack = Color(0xFF323334);
  static const transparentGray = Color(0x65323334);
  static const grayLightTransparent = Color(0x34c1c2c3);
  static const customGreen = Color(0xFF004481);
  static const transparentCustomGreen = Color(0x5525B0AC);
  static const semiTransparentCustomGreen = Color(0x1125B0AC);
  static const firstGradientColor = Color(0xFF00AFD4);
  static const secondGradientColor = Color(0xff000ffdc);
  static const gradientGray = Color(0xFFEBEBEB);
  static const whiteBackground = Color(0xFFF9F9F9);
  static const customRed = Color(0xFFFF0000);
  static const firstBlackGradient = Color(0xFF3A3737);
  static const secondBlackGradient = Color(0xFFAEAAAA);
  static const lilac = Color(0xFFBA76BF);
  static const darkWhite = Color(0xFFD1D1D1);
  static const firstPinkGradient = Color(0xFFFF5A97);
  static const secondPinkGradient = Color(0xFFEE95B6);
  static const bage = Color(0xFFF9F4D7);
  static const bage2 = Color(0xFFFFD500);
  static const semiGray = Color(0xFFE8E8E8);
  static const orange = Color(0xFFF98E47);
  static const darkYellow = Color(0xFFF9D047);
  static const redLight = Color(0xFFFB5E5E);
  static const tempColor = Color(0x25B0AC80);
  static const customOrange = Color(0xFFFFAA00);
  static const lightBlue = Color(0xFF6FA0CC);


  static const gray = Color(0xFF707070);
  static const customGray = Color(0xFFCFCFCF);
}
