// ignore_for_file: constant_identifier_names

import '../application/app_constant.dart';

class APIsUrl {
  ///TEST
  static const String BASE_URL_TEST = 'https://api.themoviedb.org/3';

  ///LIVE
  static const String BASE_URL_LIVE = 'https://api.themoviedb.org/3';

  static const String BASE_URL =
      AppConstant.isLiveServer ? BASE_URL_LIVE : BASE_URL_TEST;

  static String popularPeople = '$BASE_URL/person/popular?api_key=${AppConstant.API_KEY}&language=en-US';

  static String personDetails(int? id){
    return '$BASE_URL/person/$id?api_key=${AppConstant.API_KEY}&language=en-US';
  }

  static String personImages(int? id){
    return '$BASE_URL/person/$id/images?api_key=${AppConstant.API_KEY}';
  }

}