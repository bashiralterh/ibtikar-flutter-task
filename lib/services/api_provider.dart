// ignore_for_file: constant_identifier_names

import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ibtikar_flutter_task/model/person_images_model.dart';
import 'package:ibtikar_flutter_task/services/apis_url.dart';

import '../model/people_model.dart';
import '../model/person_details_model.dart';

enum RequestType { POST, GET, DIO, PUT, DELETE }

class ApiProvider {
  final _headers = {
    'Content-Type': 'application/json',
  };

  final _headersWithToken = {
    'Content-Type': 'application/json',
  };



   ///Base call request method
  _dataLoader(BuildContext context, RequestType requestType, String url,
      {Map<String, dynamic>? body, bool isFormData = false, bool isNeedToken = false, headerOp}) async {
    log(url);
    Response<dynamic> response;
    if (requestType == RequestType.POST || requestType == RequestType.DIO || requestType == RequestType.PUT) {
      try {
        log('body: ${json.encode(body)}');

      } catch (e) {
        log('error: $e');
      }

      response = await _dioRequest(context, url, body!,
          isFormData: isFormData,
          requestType: requestType,
          header: (headerOp != null) ? headerOp : ((isNeedToken) ? _headersWithToken : _headers));
    } else {
      response = await _dioGetRequest(context, url,
          header: (headerOp != null) ? headerOp : ((isNeedToken) ? _headersWithToken : _headers));
    }

    log('response: ${response.toString()}');

    if (response.headers["authorization"] != null) {
      ///Save token
    }

    if (response.statusCode! >= 200 && response.statusCode! <= 300) {
      return response.toString();
    } else if (response.statusCode == 401) {
      log('Not Auth ${response.statusCode}');
      Fluttertoast.showToast(msg: 'You should to login again');
    } else {
      log('response: ${response.data['message']}');
      String msg = response.data['message'] ?? 'Something went wrong';
      Fluttertoast.showToast(msg: msg);

      return null;
    }
  }


  _dioRequest(BuildContext context, String url, Map<String, dynamic> body,
      {Map<String, String>? header, bool isFormData = false, RequestType requestType = RequestType.DIO}) async {
    var dio = Dio();
    header ??= _headers;
    log('headers: ${json.encode(header)}');

    try {
      var _formData = FormData.fromMap(body);
      if (isFormData) {
        log('formdata: ${_formData.fields}');
      }
      Response response;
      if (requestType == RequestType.PUT) {
        response = await dio.put(url,
            data: isFormData ? _formData : json.encode(body),
            options: Options(headers: header, responseType: ResponseType.json));
      } else {
        response = await dio.post(url,
            data: isFormData ? _formData : json.encode(body),
            options: Options(headers: header, responseType: ResponseType.json));
      }
      return response;
    } on DioError catch (error) {
      log('error request:-- ${error.response?.statusCode}');
      log('res: ${error.response}');

      // error.response.
      if (error.response != null) {
        return error.response;
      } else {
        if (error.type == DioErrorType.connectTimeout ||
            error.type == DioErrorType.sendTimeout ||
            error.type == DioErrorType.receiveTimeout) {}
        log('request error: ${error.error}');
      }
    }
  }

  _dioGetRequest(BuildContext context, String url, {Map<String, String>? header}) async {
    var dio = Dio();
    header ??= {'Content-Type': 'application/json', 'accept': 'application/json'};
    log('headers: ${json.encode(header)}');

    try {
      final response = await dio.get(url, options: Options(headers: header, responseType: ResponseType.json));
      return response;
    } on DioError catch (error) {
      log('error request:-- $error');
      log('res: ${error.response}');

      if (error.response != null) {
        return error.response;
      } else {
        if (error.type == DioErrorType.connectTimeout ||
            error.type == DioErrorType.sendTimeout ||
            error.type == DioErrorType.receiveTimeout) {}
        log('request error: ${error.error}');
      }
    }
  }


  downloadFile(String url,String path) async {
    var dio = Dio();
    log('Downloading.. $url');
    Fluttertoast.showToast(msg: 'Downloading...');
    log('File Path.. $path');
    var response = await dio.download(url, path);
    log('Done.. ${response.data} ,${response.statusMessage} , ${response.statusCode}  ');
    Fluttertoast.showToast(msg: 'Downloaded successfully in $path',toastLength: Toast.LENGTH_LONG);
  }

  Future<PeopleModel> getPopularPeople(BuildContext context, int pageNumber) async {
    var response = await _dataLoader(
      context,
      RequestType.GET,
      '${APIsUrl.popularPeople}&page=$pageNumber',
    );

    PeopleModel _peopleModel = PeopleModel.fromJson(jsonDecode(response));

    return _peopleModel;
  }

  Future<PersonDetailsModel> getPersonDetails(BuildContext context, int? id) async {
    var response = await _dataLoader(
      context,
      RequestType.GET,
      APIsUrl.personDetails(id),
    );

    return PersonDetailsModel.fromJson(jsonDecode(response));
  }

  Future<PersonImagesModel> getPersonImages(BuildContext context, int? id) async {
    var response = await _dataLoader(
      context,
      RequestType.GET,
      APIsUrl.personImages(id),
    );

    return PersonImagesModel.fromJson(jsonDecode(response));
  }
}

final apiProvider = ApiProvider();
