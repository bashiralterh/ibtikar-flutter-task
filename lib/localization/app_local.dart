import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../managers/data_store.dart';

class AppLocalizations {
  AppLocalizations(this.locale);

  final Locale locale;

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations) ??
        AppLocalizations(Locale(dataStore.lang));
  }

  Map<String, String> _sentences = {};

  Future<bool> load() async {
    var data =
    await rootBundle.loadString('assets/lang/${locale.languageCode}.json');
    try {
      Map<String, dynamic> _result = json.decode(data);

      _sentences = {};
      _result.forEach((String key, dynamic value) {
        _sentences[key] = value.toString();
      });
    }catch(e){
      log('Error: Load locale: $e');
    }
    return true;
  }

  String trans(String key) {
    return _sentences[key] ?? key;
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['ar', 'en'].contains(locale.languageCode);

  @override
  Future<AppLocalizations> load(Locale locale) async {
    var localizations = AppLocalizations(locale);
    await localizations.load();

    log('Load ${locale.languageCode}');

    return localizations;
  }

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}
