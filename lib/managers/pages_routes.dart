import 'package:flutter/cupertino.dart';

class PagesRoutes {
  static goToPage(BuildContext context, Widget page, {bool withReplacement = false}) {
    if (withReplacement) {
      Navigator.pushReplacement(context, CupertinoPageRoute(builder: (context) => page));
    } else {
      Navigator.push(context, CupertinoPageRoute(builder: (context) => page));
    }
  }
}
