import 'dart:convert';

import 'package:ibtikar_flutter_task/model/people_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../application/enums.dart';

class DataStore {
  String _lang = 'en';

  get lang {
    return _lang;
  }

  Future<bool> setLang(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = value;
    return prefs.setString(DataStoreKeys.lang.toString(), value);
  }

  Future<String> getLang() async {
    final prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString(DataStoreKeys.lang.toString()) ?? 'en';
    return _lang;
  }

  Future<bool> setPeople(PeopleModel peopleModel, int pageNumber) async {
    final prefs = await SharedPreferences.getInstance();
    var object = peopleModel.toJson();
    return prefs.setString('${DataStoreKeys.people.toString()}-$pageNumber', json.encode(object));
  }

  Future<PeopleModel?> getPeople(int pageNumber) async {
    final prefs = await SharedPreferences.getInstance();

    try {
      var object = json.decode(prefs.getString('${DataStoreKeys.people.toString()}-$pageNumber') ?? '');

      var _people = PeopleModel.fromJson(await object);
      return _people;
    } catch (e) {
      return null;
    }
  }

  void clearCache() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }
}

final dataStore = DataStore();
