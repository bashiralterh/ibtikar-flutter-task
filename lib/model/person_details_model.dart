// To parse this JSON data, do
//
//     final personDetailsModel = personDetailsModelFromJson(jsonString);

import 'dart:convert';

PersonDetailsModel personDetailsModelFromJson(String str) => PersonDetailsModel.fromJson(json.decode(str));

String personDetailsModelToJson(PersonDetailsModel data) => json.encode(data.toJson());

class PersonDetailsModel {
  PersonDetailsModel({
    required this.adult,
    required this.alsoKnownAs,
    required this.biography,
    required this.birthday,
    required this.deathday,
    required this.gender,
    required this.homepage,
    required this.id,
    required this.imdbId,
    required this.knownForDepartment,
    required this.name,
    required this.placeOfBirth,
    required this.popularity,
    required this.profilePath,
  });

  final bool adult;
  final List<String> alsoKnownAs;
  final String biography;
  final String birthday;
  final dynamic deathday;
  final int gender;
  final dynamic homepage;
  final int id;
  final String imdbId;
  final String knownForDepartment;
  final String name;
  final String placeOfBirth;
  final double popularity;
  final String profilePath;

  factory PersonDetailsModel.fromJson(Map<String, dynamic> json) => PersonDetailsModel(
        adult: json["adult"] ?? '',
        alsoKnownAs: json["also_known_as"] == null ? [] : List<String>.from(json["also_known_as"].map((x) => x)),
        biography: json["biography"] ?? '',
        birthday: json["birthday"] ?? '',
        deathday: json["deathday"] ?? '',
        gender: json["gender"] ?? 0,
        homepage: json["homepage"] ?? '',
        id: json["id"] ?? 0,
        imdbId: json["imdb_id"] ?? 0,
        knownForDepartment: json["known_for_department"] ?? '',
        name: json["name"] ?? '',
        placeOfBirth: json["place_of_birth"] ?? '',
        popularity: json["popularity"]?.toDouble() ?? 0.0,
        profilePath: json["profile_path"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "adult": adult,
        "also_known_as": List<dynamic>.from(alsoKnownAs.map((x) => x)),
        "biography": biography,
        "birthday": birthday,
        "deathday": deathday,
        "gender": gender,
        "homepage": homepage,
        "id": id,
        "imdb_id": imdbId,
        "known_for_department": knownForDepartment,
        "name": name,
        "place_of_birth": placeOfBirth,
        "popularity": popularity,
        "profile_path": profilePath,
      };
}
