// To parse this JSON data, do
//
//     final personImagesModel = personImagesModelFromJson(jsonString);

import 'dart:convert';

PersonImagesModel personImagesModelFromJson(String str) => PersonImagesModel.fromJson(json.decode(str));

String personImagesModelToJson(PersonImagesModel data) => json.encode(data.toJson());

class PersonImagesModel {
  PersonImagesModel({
    required this.id,
    required this.images,
  });

  final int id;
  final List<ImageItem> images;

  factory PersonImagesModel.fromJson(Map<String, dynamic> json) => PersonImagesModel(
        id: json["id"] ?? 0,
        images: json["profiles"] == null ? [] : List<ImageItem>.from(json["profiles"].map((x) => ImageItem.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "profiles": List<dynamic>.from(images.map((x) => x.toJson())),
      };
}

class ImageItem {
  ImageItem({
    required this.aspectRatio,
    required this.height,
    required this.iso6391,
    required this.filePath,
    required this.voteAverage,
    required this.voteCount,
    required this.width,
  });

  final double aspectRatio;
  final int height;
  final dynamic iso6391;
  final String filePath;
  final double voteAverage;
  final int voteCount;
  final int width;

  factory ImageItem.fromJson(Map<String, dynamic> json) => ImageItem(
        aspectRatio: json["aspect_ratio"]?.toDouble() ?? 0.0,
        height: json["height"] ?? 0,
        iso6391: json["iso_639_1"] ?? '',
        filePath: json["file_path"] ?? '',
        voteAverage: json["vote_average"]?.toDouble() ?? 0.0,
        voteCount: json["vote_count"] ?? 0,
        width: json["width"] ?? 0,
      );

  Map<String, dynamic> toJson() => {
        "aspect_ratio": aspectRatio,
        "height": height,
        "iso_639_1": iso6391,
        "file_path": filePath,
        "vote_average": voteAverage,
        "vote_count": voteCount,
        "width": width,
      };
}
